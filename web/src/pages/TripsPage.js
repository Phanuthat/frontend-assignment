import React, { Component } from "react"
import { Trips } from "../components/Trips/Trips"
export default class TripsPage extends Component {
  render() {
    return <Trips data-testid='trips-page' />
  }
}
