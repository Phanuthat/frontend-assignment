import React from "react"
import "@testing-library/jest-dom"
import { render } from "@testing-library/react"
import { Trips } from "../components/Trips/Trips"
test("app render have element", async () => {
  const { getByTestId } = render(<Trips />)
  const tripsPage = getByTestId("trips-page")
  const input = getByTestId("input-icon")
  const header = getByTestId("header")
  expect(tripsPage).toContainElement(header)
  expect(tripsPage).toContainElement(input)
  afterEach(() => {
    const cardList = getByTestId("card-list")
    expect(tripsPage).toContainElement(cardList)
  })
})
