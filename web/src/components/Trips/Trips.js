import React, { useEffect, useState } from "react"
import axios from "axios"
import searchIcon from "../../static/searchIcon.svg"
import CardList from "../../common/CardList/CardList"
import InputWithIcon from "../../common/InputWithIcon/InputWithIcon"
import { Container, Header } from "./StyleTrips"

export const Trips = (props) => {
  const [trips, setTrips] = useState([])
  const [keywords, setKeywords] = useState("")
  const [isLoading, setLoading] = useState(true)
  useEffect(() => {
    getTripData()
  }, [])

  const getTripData = () => {
    axios
      .get("http://localhost:9000/api/trips")
      .then((res) => {
        const tripsData = res.data
        setTrips(tripsData)
        setLoading(false)
      })
      .catch((err) => console.log("error", err))
  }

  const searchTrips = async (text) => {
    try {
      const trips = await axios.get(
        `http://localhost:9000/api/trips?keywords?=${text}`,
      )
      const tripsData = trips.data
      setTrips(tripsData)
      setLoading(false)
    } catch (err) {
      console.log("error", err)
    }
  }

  const handleInputChange = (event) => {
    event.preventDefault()
    const value = event.target.value
    setKeywords(value)
  }

  const onEnter = (event) => {
    const { key } = event
    const value = event.target.value
    if (key === "Enter" && value) {
      searchTrips(value)
    }

    if (key === "Enter" && !value) {
      getTripData()
    }
  }

  return (
    <Container data-testid='trips-page'>
      <Header data-testid='header'>เที่ยวไหนดี</Header>
      <InputWithIcon
        iconRight={true}
        icon={searchIcon}
        width={800}
        onChange={handleInputChange}
        value={keywords}
        onKeyPress={onEnter}
        onClickIcon={() => searchTrips(keywords)}
        placeholder='หาที่เที่ยวแล้วไปกัน...'
      />
      {isLoading ? (
        <p>Loading ...</p>
      ) : trips.length ? (
        <CardList
          data-testid='card-list'
          listData={trips}
          onClickTag={searchTrips}
          numberLineDescription={2}
          onClickTitle={(url) => (window.location.href = url)}
        />
      ) : (
        <p>ไม่พบข้อมูล</p>
      )}
    </Container>
  )
}
