import React from "react"
import { render } from "@testing-library/react"
import App from "./App"

test("app render have element", () => {
  const { getByTestId } = render(<App />)
  const container = getByTestId("container")
  const tripsPage = getByTestId("trips-page")
  expect(container).toContainElement(tripsPage)
})
