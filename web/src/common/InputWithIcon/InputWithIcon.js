import React, { Component } from "react"
import PropTypes from "prop-types"
import { Contaniner, Image, Input } from "./StyledInput"
export default class InputWithIcon extends Component {
  render() {
    const {
      onChange,
      onClickIcon,
      onKeyPress,
      icon,
      width,
      value,
      placeholder,
      iconRight,
    } = this.props
    return (
      <Contaniner width={width}>
        <Input
          data-testid='input-icon'
          onChange={onChange}
          value={value}
          onKeyPress={onKeyPress}
          placeholder={placeholder}
        />
        {icon && (
          <Image
            data-testid='position-image'
            src={icon}
            onClick={(e) => onClickIcon(e)}
            iconRight={iconRight}
          />
        )}
      </Contaniner>
    )
  }
}
InputWithIcon.propTypes = {
  icon: PropTypes.string,
  onChange: PropTypes.func,
  onClickIcon: PropTypes.func,
  onKeyPress: PropTypes.func,
  width: PropTypes.number,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  iconRight: PropTypes.bool,
}

InputWithIcon.defaultProps = {
  icon: "",
  onChange: () => {},
  onClickIcon: () => {},
  onKeyPress: () => {},
  width: 800,
  placeholder: "",
  value: "",
  iconRight: true,
}
