import React from "react"
import "./App.css"
import TripsPage from "../src/pages/TripsPage"
function App() {
  return (
    <div data-testid='container'>
      <TripsPage data-testid='trips-page'/>
    </div>
  )
}

export default App
